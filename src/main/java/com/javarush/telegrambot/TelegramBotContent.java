package com.javarush.telegrambot;

public class TelegramBotContent {
    public static final String STEP_1_TEXT = """
                *WELCOME TO LEVEL ONE!*
                                                                         
                You wake up and realize that you are a cat. Good morning, but what are these strange feelings? Your stomach is growling with intense hunger. There's no food in the apartment, except maybe in the closed refrigerator. What should you do?

                Time to remember how to hack a digital refrigerator. Let's open the book "Advanced Hacking for Cats":

                    1. Complete the first task, then turn the page!
                    2. Level up and move on to the most interesting part!
                    3. Complete tasks to earn respect among all the local cats!

                *Domestic Cat, level one.*
                Hack the digital refrigerator and complete the first task!
            """;

    public static final String STEP_2_TEXT = """
                *WELCOME TO LEVEL TWO!*
                                       
                Woohoo! You defeated the refrigerator. You gain +20 glory!
                
                _Accumulated: 20 glory._
                
                Choose your reward:
            """;

    public static final String STEP_3_TEXT = """
                *IT Cat, level two.*
                Suddenly, a buzzing robot vacuum cleaner crawls out from around the corner. It's time for revenge!
                Complete the second task and hack it!
            """;

    public static final String STEP_4_TEXT = """
                *WELCOME TO LEVEL THREE!*
                                       
                You dealt with the robot vacuum cleaner! You gain +30 glory!
                
                _Accumulated: 70 glory._
                
                Choose your reward:
            """;

    public static final String STEP_5_TEXT = """
                *Robo-Tamer Cat, level three.*
                Ho-ho! An abandoned GoPro is found on the wardrobe!
                Complete the third task — put it on and turn it on!
            """;

    public static final String STEP_6_TEXT = """
                *WELCOME TO LEVEL FOUR!*
                                       
                You put on the GoPro! You gain +40 glory!
                                       
                _Accumulated: 140 glory._
                
                Choose your reward:
            """;

    public static final String STEP_7_TEXT = """
                *Video-Blogger Cat, level four.*
                Now you need to upload the footage to the computer.
                Complete the fourth task — hack the computer password!
            """;

    public static final String STEP_8_TEXT = """
                *WELCOME TO LEVEL FIVE!*
                
                You defeated the computer! You gain +50 glory!
                
                _Accumulated: 230 glory._
            """;

    public static final String FINAL_TEXT = """
                *Hacker Cat, level five.*
                The day wasn't wasted: the hacker cat completed the mission and earned respect among the local stray cats.
            """;

}