package com.javarush.telegrambot;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.Map;

import static com.javarush.telegrambot.TelegramBotContent.*;

public class TelegramBot extends MultiSessionTelegramBot {
    public static final String NAME = "AwesomeJavaBot_bot";
    public static final String TOKEN = "6964704307:AAH3FueE6KYEKK10TIQJW5l7zMjvth6XHZo";

    public TelegramBot() {
        super(NAME, TOKEN);
    }
    @Override
    public void onUpdateEventReceived(Update updateEvent) {
        setUserGlory ( 0 );
        if (getMessageText ().equals ( "/start" )) {
            sendTextMessageAsync (STEP_1_TEXT, Map.of ( "Hacking a refrigerator", "step-1_btn" ));
        }

        if (getCallbackQueryButtonKey ().equals ( "step-1_btn" )){
            addUserGlory ( 20 );
            sendTextMessageAsync ( STEP_2_TEXT,
                    Map.of ("Take the sausage! +20 fame", "step-2_btn",
                            "Get the fish! +20 fame", "step-2_btn",
                            "Dump the jar of cucumbers! +20 fame", "step-2_btn" ));
        }

        if (getCallbackQueryButtonKey ().equals ( "step-2_btn" )){
            addUserGlory ( 20 );
            sendTextMessageAsync ( STEP_3_TEXT,
                    Map.of ("Hacking a robot vacuum cleaner", "step-3_btn"));
        }

        if (getCallbackQueryButtonKey ().equals ( "step-3_btn" )){
            addUserGlory ( 20 );
            sendTextMessageAsync ( STEP_4_TEXT,
                    Map.of ("Send a robot vacuum cleaner for food! +30 fame", "step-4_btn",
                            "Ride a robot vacuum cleaner! +30 fame", "step-4_btn",
                            "Run away from the robot vacuum cleaner! +30 fame", "step-4_btn" ));
        }
        if (getCallbackQueryButtonKey().equals("step-4_btn")) {
            addUserGlory(30);
            sendTextMessageAsync(STEP_5_TEXT,
                    Map.of("Put on the GoPro!", "step-5_btn"));
        }
        if (getCallbackQueryButtonKey().equals("step-5_btn")) {
            addUserGlory(40);
            sendTextMessageAsync(STEP_6_TEXT,
                    Map.of("Run on rooftops with the GoPro! +40 glory", "step-6_btn",
                            "Attack other cats with the GoPro! +40 glory", "step-6_btn",
                            "Attack dogs with the GoPro! +40 glory", "step-6_btn"));
        }
        if (getCallbackQueryButtonKey().equals("step-6_btn")) {
            addUserGlory(40);
            sendTextMessageAsync(STEP_7_TEXT,
                    Map.of("Hack the computer password", "step-7_btn"));
        }
        if (getCallbackQueryButtonKey().equals("step-7_btn")) {
            addUserGlory(50);
            sendTextMessageAsync(STEP_8_TEXT,
                    Map.of("Complete level five", "step-8_btn"));
        }
        if (getCallbackQueryButtonKey().equals("step-8_btn")) {sendTextMessageAsync(FINAL_TEXT,
                    Map.of("End of the game", "final_btn"));
        }
    }


    public static void main(String[] args) throws TelegramApiException {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(new TelegramBot ());
    }
//        if (getMessageText().equals("/buy")) {
//            sendTextMessageAsync("Asta la vista, baby");
//        }
//
//        if (getMessageText().contains("/start")) {
//            sendTextMessageAsync("What's your favorite animal?",
//                    Map.of("Cat", "cat", "Dog", "dog"));
//        }
//
//        if (getCallbackQueryButtonKey ().equals ( "cat" )){
//            sendPhotoMessageAsync ( "step_4_pic" );
//        }
//
//        if (getCallbackQueryButtonKey ().equals ( "dog" )){
//            sendPhotoMessageAsync ( "step_6_pic" );
//        }
    }